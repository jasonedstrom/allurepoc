package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import support.PropertyHandler;

public class LoginPage extends BasePageModel
{
	/** The URL for the page. */
	private static final String pageUrl = PropertyHandler.getWebSiteBase() + "Static/Training1/loginpage.html";

	/** Username Field Selector. */
	private static final By usernameInput = By.cssSelector("#UserName");

	/** Password Field Selector. */
	private static final By passwordInput = By.cssSelector("#Password");

	/** Login Error message Selector. */
	private static final By loginError = By.cssSelector("#LoginError");

	/** Login Button Selector. */
	private static final By loginButton = By.cssSelector("#Login");

	/** Logged in message Selector */
	private static final By welcomeMessage = By.cssSelector("#WelcomeMessage");

	/**
	 * Constructor
	 * @param testObject The test object
	 */
	public LoginPage(WebDriver testObject)
	{
		super(testObject);
	}

	/**
	 * Opens the login page
	 * @throws InterruptedException If the thread is interrupted
	 */
	public void openLoginPage() throws InterruptedException
	{
		this.testObject.get(pageUrl);
		Thread.sleep(this.driverWaitTime);
	}

	/**
	 * Sends username and password to input boxes
	 * @param username The username that is used
	 * @param password The password that is used
	 * @throws InterruptedException If the thread is interrupted
	 */
	public void enterCredentials(String username, String password) throws InterruptedException
	{
		this.openLoginPage();
		Thread.sleep(this.driverWaitTime);
		this.testObject.findElement(usernameInput).sendKeys(username);
		this.testObject.findElement(passwordInput).sendKeys(password);
	}

	/**
	 * Logins to the loginpage with the credentials provided
	 * @param username The username provided for login
	 * @param password The password provided for login
	 * @return The loginpage after logging in
	 * @throws InterruptedException If the thread is interrupted
	 */
	public LoginPage loginWithValidCredentials(String username, String password) throws InterruptedException
	{
		this.enterCredentials(username, password);
		Thread.sleep(this.driverWaitTime);
		this.testObject.findElement(loginButton).click();

		LoginPage loggedInPage = new LoginPage(this.testObject);
		return loggedInPage;
	}

	/**
	 * Logins to the loginpage with invalid credentials
	 * @param username The username provided for login
	 * @param password The password provided for login
	 * @throws InterruptedException If the thread is interrupted
	 */ 
	public void loginWithInvalidCredentials(String username, String password) throws InterruptedException
	{
		this.enterCredentials(username, password);
		Thread.sleep(this.driverWaitTime);
		this.testObject.findElement(loginButton).click();
		Thread.sleep(this.driverWaitTime);
		this.testObject.findElement(loginError);
	}

	/**
	 * Verifies if the user is viewing the logged in page
	 * @return If the user is viewing the logged in page or not
	 * @throws InterruptedException If the thread is interrupted
	 */
	public boolean isLoggedIn() throws InterruptedException
	{
		return this.testObject.findElement(welcomeMessage).isDisplayed();
	}
	
	@Override
	public boolean isPageLoaded()
	{
		return this.testObject.findElement(usernameInput).isDisplayed();
	}
}
