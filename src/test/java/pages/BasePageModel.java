package pages;

import org.openqa.selenium.WebDriver;

import support.PropertyHandler;

/**
 * The type Base page model.
 */
public abstract class BasePageModel 
{
	/** The Test object */
	protected WebDriver testObject;

	/** the wait time for the web driver */
	protected int driverWaitTime = PropertyHandler.getWaitTime();

	/**
	 * Instantiates a new Base page model
	 * @param testObject the test object
	 */
	public BasePageModel(WebDriver testObject) 
	{
		this.testObject = testObject;
	}

	/**
	 * Returns true if the page is loaded
	 * @return If the page is loaded or not
	 */
	public abstract boolean isPageLoaded();
}