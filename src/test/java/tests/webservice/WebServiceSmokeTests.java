package tests.webservice;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import support.PropertyHandler;
import support.TestHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * WebService smoke tests using httpclient
 * to see if the endpoints are available
 * @author RobertW
 *
 */

public class WebServiceSmokeTests
{	
	/** List of all active closeable http clients */
	private static List<CloseableHttpClient> clientList = new ArrayList<CloseableHttpClient>();

	/** List of all active closeable http responses */
	private static List<CloseableHttpResponse> responseList = new ArrayList<CloseableHttpResponse>();

	/** base uri for making requests */
	private static final String baseUri = PropertyHandler.getWebSiteBase();

	/**
	 * Tests the home page endpoint
	 */
	@Test

	public void testHomePageEndpoint()
	{
		CloseableHttpClient client = TestHelper.getHttpClient();
		clientList.add(client);

		HttpGet homepageGet = new HttpGet(baseUri);

		CloseableHttpResponse response = null;

		try 
		{
			response = client.execute(homepageGet);
			responseList.add(response);
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);
	}

	/**
	 * Tests the contact page endpoint
	 */
	@Test

	public void testContactPageEndpoint()
	{
		CloseableHttpClient client = TestHelper.getHttpClient();
		clientList.add(client);

		HttpGet contactpageGet = new HttpGet(String.format("%s/Home/Contact", baseUri));

		CloseableHttpResponse response = null;

		try 
		{
			response = client.execute(contactpageGet);
			responseList.add(response);
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);
	}

	/**
	 * Tests the login page endpoint
	 */
	@Test

	public void testLoginPageEndpoint()
	{
		CloseableHttpClient client = TestHelper.getHttpClient();
		clientList.add(client);

		HttpGet loginpageGet = new HttpGet(String.format("%s/Static/Training1/loginpage.html", baseUri));

		CloseableHttpResponse response = null;

		try 
		{
			response = client.execute(loginpageGet);
			responseList.add(response);
		} 
		catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);

	}

	/**
	 * Cleanup strategy to close resources after test
	 * completion
	 */
	@AfterClass
	public void cleanup() {
		/*if (!clientList.isEmpty())
		{
			clientList.stream().forEach(x -> 
			{
				try 
				{
					x.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			});
		}

		clientList.clear();

		if (!responseList.isEmpty())
		{
			responseList.stream().forEach(x -> 
			{
				try
				{
					x.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			});
		}

		responseList.clear();
	*/
	}
}
