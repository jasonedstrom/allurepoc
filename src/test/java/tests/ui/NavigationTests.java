package tests.ui;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import pages.ContactPage;
import pages.HomePage;
import pages.LoginPage;
import support.PropertyHandler;
import support.SeleniumWebDriverOptions;
import support.TestHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Tests Navigation throughout the webpage
 * @author RobertW
 *
 */
public class NavigationTests
{


	/** List of all active drivers */
	private static List<WebDriver> driverList = new ArrayList<WebDriver>();

	private String username = PropertyHandler.getValue("Username");
	private String password = PropertyHandler.getValue("Password");

	/**
	 * Tests invalid login when the web driver sends
	 * invalid credentials on the login page
	 * @throws InterruptedException If the thread is interrupted
	 */
	@Test(expectedExceptions = NoSuchElementException.class)
	public void invalidLoginTest() throws InterruptedException 
	{
		String username = "zed";
		String password = "fred";

		WebDriver driver = TestHelper.getSeleniumDriver(SeleniumWebDriverOptions.Chrome);
		driverList.add(driver);

		LoginPage login = new LoginPage(driver);
		login.openLoginPage();

		login.loginWithInvalidCredentials(username, password);
		Assert.assertFalse(login.isLoggedIn());
	}

	/**
	 * Tests successful login when web driver sends in
	 * correct credentials at the login page
	 * @throws InterruptedException If the thread is interrupted
	 */
	@Test
	public void validLoginTest() throws InterruptedException
	{
		WebDriver driver = TestHelper.getSeleniumDriver(SeleniumWebDriverOptions.Chrome);
		driverList.add(driver);

		LoginPage login = new LoginPage(driver);
		login.openLoginPage();

		login.loginWithValidCredentials(this.username, this.password);
		Assert.assertTrue(login.isLoggedIn());
	}

	/**
	 * Tests if the web driver can open the home page
	 * @throws InterruptedException If the thread is interrupted
	 */
	@Test 
	public void openHomePage() throws InterruptedException
	{
		WebDriver driver = TestHelper.getSeleniumDriver(SeleniumWebDriverOptions.Chrome);
		driverList.add(driver);

		HomePage home = new HomePage(driver);
		home.openHomePage();
		Assert.assertTrue(home.isPageLoaded());
	}

	/**
	 * Tests if the web driver can find the contact page
	 * @throws InterruptedException If the thread is interrupted
	 */
	@Test
	public void openContactPage() throws InterruptedException
	{
		WebDriver driver = TestHelper.getSeleniumDriver(SeleniumWebDriverOptions.Chrome);
		driverList.add(driver);

		ContactPage contact = new ContactPage(driver);
		contact.openContactPage();

		Assert.assertTrue(contact.isPageLoaded());
	}

	/**
	 * Tests if the web driver can find the login page
	 * @throws InterruptedException If the thread is interrupted
	 */
	@Test
	public void openLoginPage() throws InterruptedException
	{
		WebDriver driver = TestHelper.getSeleniumDriver(SeleniumWebDriverOptions.Chrome);
		driverList.add(driver);

		LoginPage login = new LoginPage(driver);
		login.openLoginPage();

		Assert.assertTrue(login.isPageLoaded());
	}

	/**
	 * Cleanup strategy to close resources after test
	 * completion
	 */
	@AfterClass
	public void cleanup()
	{
		if (!driverList.isEmpty())
		{
			driverList.stream().forEach(x -> x.close());
		}
		driverList.clear();
	}
}
