package support;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestHelper 
{
	/**
	 * Gets the Selenium WebDriver
	 * @param driverOption The type of driver that will be returned
	 * @return One of the browser options that are returned
	 */
	public static WebDriver getSeleniumDriver(SeleniumWebDriverOptions driverOption)
	{

		WebDriver browser = null;
		switch(driverOption)
		{
			case Chrome:
				//System.getProperty("webdriver.chrome.driver");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--headless");
				options.addArguments("-disable-dev-shm-usage");
				options.addArguments("--no-sandbox");
				browser = new ChromeDriver(options);
				break;
			case Firefox:
				System.setProperty("webdriver.gecko.driver","src/test/resources/driver/firefox.exe");
				browser = new FirefoxDriver();
				break;
			case InternetExplorer:
				System.setProperty("webdriver.ie.driver","src/test/resources/driver/iedriver.exe");
				browser = new InternetExplorerDriver();
				break;
		}
		
		return browser;
	}
	
	/**
	 * Gets the http client 
	 * @return The http client
	 */
	public static CloseableHttpClient getHttpClient()
	{
		CloseableHttpClient client = HttpClients.createDefault();
		return client;
	}
}
